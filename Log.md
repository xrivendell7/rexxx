# LOG

## 修改日志 2023.02.22 lxw

我们运行的run.sh为
```bash
sudo chroot ./ ./shellphish-qemu-linux-arm  -E LD_PRELOAD="nvram.so ./lib/libdl.so.0" ./upnpd
```

需要qemu支持参数`-E LD_PRELOAD`
和增加chroot部分

断点 `set ipdb; ipdb.set_trace();` 打在qemu_tracer中

```bash
> /home/wsl/angr-dev/rex/archr/analyzers/qemu_tracer.py(267)_build_command()
    266
--> 267         qemu_variant = self.qemu_variant(self.target.target_os, self.target.target_arch, trace_filename is not None)
    268         qemu_path = os.path.join(self.target.tmpwd, "shellphish_qemu", qemu_variant)

ipdb> bt
  /home/wsl/angr-dev/rex/tests/test_rex.py(615)<module>()
    613     # test_linux_armel()
    614     # test_linux_armel_stacksmash_jump()
--> 615     test_netgear_upnpd_stackoverflow()

  /home/wsl/angr-dev/rex/tests/test_rex.py(581)test_netgear_upnpd_stackoverflow()
    580     print(target)
--> 581     crash = rex.Crash(target, inp)
    582     print(crash)

  /home/wsl/angr-dev/rex/rex/crash.py(651)__init__()
    650         # Initialize
--> 651         self._initialize()
    652

  /home/wsl/angr-dev/rex/rex/crash.py(1039)_initialize()
   1038         # concrete tracing for coredump file?
-> 1039         self.concrete_trace()
   1040         self.create_project()

  /home/wsl/angr-dev/rex/rex/crash.py(433)concrete_trace()
    432         # NOTE: concrete_trace
--> 433         self.trace_result, self.core_registers = self.tracer.concrete_trace(testcase, channel,
    434                                                                              self.pre_fire_hook,

  /home/wsl/angr-dev/rex/rex/crash_tracer/full_tracer.py(17)concrete_trace()
     16         # Run qemu to collect coredump file for further analysis
---> 17         r = self.tracer_bow.fire(testcase=testcase, channel=channel, save_core=True, record_magic=self._is_cgc,
     18                                  pre_fire_hook=pre_fire_hook, delay=delay, actions=actions, taint=taint)

  /home/wsl/angr-dev/rex/archr/analyzers/__init__.py(65)fire()
     64
---> 65         with self.fire_context(*args, **kwargs) as flight:
     66             if delay:

  /usr/lib/python3.8/contextlib.py(113)__enter__()
    112         try:
--> 113             return next(self.gen)
    114         except StopIteration:

  /home/wsl/angr-dev/rex/archr/analyzers/qemu_tracer.py(106)fire_context()
    105
--> 106             target_cmd = self._build_command(
    107                 trace_filename=target_trace_filename,

> /home/wsl/angr-dev/rex/archr/analyzers/qemu_tracer.py(267)_build_command()
    266
--> 267         qemu_variant = self.qemu_variant(self.target.target_os, self.target.target_arch, trace_filename is not None)
    268         qemu_path = os.path.join(self.target.tmpwd, "shellphish_qemu", qemu_variant)
```

注意到有QEMUTracerAnalyzer的部分需要可以输入`ld_preload`链接上我们的nvraw.so

找到例子https://github.com/angr/angr/issues/2786

还有点问题在

```bash
DEBUG   | 2023-02-22 21:24:23,441 | archr.targets | Running command: 'rm' '-rf' '/tmp/tracer_target_k8w_c298'
Traceback (most recent call last):
  File "test_rex.py", line 616, in <module>
    test_netgear_upnpd_stackoverflow()
  File "test_rex.py", line 582, in test_netgear_upnpd_stackoverflow
    crash = rex.Crash(target, inp)
  File "/home/wsl/angr-dev/rex/rex/crash.py", line 651, in __init__
    self._initialize()
  File "/home/wsl/angr-dev/rex/rex/crash.py", line 1039, in _initialize
    self.concrete_trace()
  File "/home/wsl/angr-dev/rex/rex/crash.py", line 433, in concrete_trace
    self.trace_result, self.core_registers = self.tracer.concrete_trace(testcase, channel,
  File "/home/wsl/angr-dev/rex/rex/crash_tracer/full_tracer.py", line 17, in concrete_trace
    r = self.tracer_bow.fire(testcase=testcase, channel=channel, save_core=True, record_magic=self._is_cgc,
  File "/home/wsl/angr-dev/rex/archr/analyzers/__init__.py", line 71, in fire
    self._fire_testcase(flight, channel=channel)
  File "/usr/lib/python3.8/contextlib.py", line 120, in __exit__
    next(self.gen)
  File "/home/wsl/angr-dev/rex/archr/analyzers/qemu_tracer.py", line 156, in fire_context
    raise QEMUTracerError("the target didn't crash inside qemu or no corefile was created!" +
archr.analyzers.qemu_tracer.QEMUTracerError: the target didn't crash inside qemu or no corefile was created!Make sure you launch it correctly!
```

在qemu_tracer.py 中的下断点，然后看到底哪里出了问题？

根据log单独执行出问题的命令

```bash
❯ /tmp/archr_local_ba2i2lly/shellphish_qemu/fire /tmp/archr_local_ba2i2lly/shellphish_qemu/shellphish-qemu-linux-arm -C /tmp/tracer_target_e6op2npe -d nochain,exec,page,strace -D /tmp/tracer-q9q0ejd_.trace -E LD_PRELOAD="nvram.so ./lib/libdl.so.0" -- /home/wsl/angr-dev/rex/tests/../my_binaries/ld-linux.so.3 --library-path /home/wsl/angr-dev/rex/tests/../my_binaries /home/wsl/angr-dev/rex/tests/../my_binaries/upnpd
/home/wsl/angr-dev/rex/tests/../my_binaries/upnpd: error while loading shared libraries: /home/wsl/angr-dev/rex/tests/../my_binaries/upnpd: cannot open shared object file: No such file or directory
```
看起来都是路径的问题

修改好了路径

```bash
❯ sudo chroot /home/wsl/angr-dev/rex/my_binaries/CVE-2021-27329/ /tmp/archr_local_ndmrz3an/shellphish_qemu/fire /tmp/archr_local_ndmrz3an/shellphish_qemu/shellphish-qemu-linux-arm -C /tmp/tracer_target_hktn0hhg -d nochain,exec,page,strace -D /tmp/tracer-jkw14te9.trace -E LD_PRELOAD="/home/wsl/angr-dev/rex/my_binaries/CVE-2021-27329/nvram.so /home/wsl/angr-dev/rex/my_binaries/CVE-2021-27329/lib/libdl.so.0" -- /home/wsl/angr-dev/rex/tests/../my_binaries/CVE-2021-27329/ld-linux.so.3 --library-path /home/wsl/angr-dev/rex/tests/../my_binaries/CVE-2021-27329 /home/wsl/angr-dev/rex/tests/../my_binaries/CVE-2021-27329/upnpd
chroot: failed to run command ‘/tmp/archr_local_ndmrz3an/shellphish_qemu/fire’: No such file or directory
```


这下看起来比较麻烦，我们需要chroot才可以，所以要将fire,shellphish-qemu-linux-arm文件都放到我们的Target_bin 的目录下，然后chroot应该就完事了，这就需要改代码了

最好的方法是将CVE-2021-27329文件夹目录下的所有内容移动到`/tmp/xxxxxxxxxx` 新建的目录下，执行得到trace后就丢掉

改了代码后的运行命令为

```bash
/tmp/tracer_target_sfeewoif/CVE-2021-27329
DEBUG   | 2023-02-22 22:32:08,724 | archr.analyzers.qemu_tracer | launch QEMU with command: /tmp/archr_local_t696k92p/shellphish_qemu/fire /tmp/archr_local_t696k92p/shellphish_qemu/shellphish-qemu-linux-arm -C /tmp/tracer_target_sfeewoif -d nochain,exec,page,strace -D /tmp/tracer-rr9j_omm.trace -E LD_PRELOAD="nvram.so ./lib/libdl.so.0" -- ./upnpd
```

如果在前面加chroot的话，上面的所有路径都要变成本地的,emmm

全部把路径改成本地，并且也成功让这样的命令
注意要在chroot的时候指定好路径
运行一个类似这样的命令

```bash
sudo chroot /tmp/tracer_target_t02khqkb/CVE-2021-27329 ./shellphish-qemu-linux-arm -C /tmp/tracer_target_t02khqkb -d nochain,exec,page,strace -D /tmp/tracer-3ml1u1mo.trace -E LD_PRELOAD="./nvram.so ./lib/libdl.so.0" -- ./upnpd
```
这样就不需要`cd`或者在python中chdir了
跑起来了

可以在跑起来之后暂停一下确认

遇到了新的问题在于action
我们不是通过stdin管道给他输入，应该是通过访问相应的端口给他输入，这样来看的话，还需要更改触发方式
这样的话，它这样确认crash的方法是有问题的，需要大改...


## 修改日志 2023.02.23 lxw
找一下具体shellphish-qemu-linux-arm执行的位置

先看为什么rex执行的时候总是报错如下
```bash
DEBUG   | 2023-02-23 09:29:21,881 | archr.target.flight | stderr:
./upnpd: can't load library './lib/libdl.so.0"'
```
但是bash执行该命令没有问题

```bash
sudo chroot /tmp/tracer_target_ulcu65yh/CVE-2021-27329 ./shellphish-qemu-linux-arm -C /tmp/tracer_target_ulcu65yh -d nochain,exec,page,strace -D /tmp/tracer-ms4arzt6.trace -E LD_PRELOAD="./nvram.so ./lib/libdl.so.0" -- ./upnpd
```
这个命令执行是没问题的啊，怎么回事
发现是用subprocess.Popen执行的命令
单独用subprocess.Popen来执行试一下发现还是有问题

```python
In [4]: subprocess.Popen(['sudo', 'chroot', '/tmp/tracer_target_txip6ek6/CVE-2021-27329', './
   ...: shellphish-qemu-linux-arm', '-C', '/tmp/tracer_target_txip6ek6', '-d', 'nochain,exec,
   ...: page,strace', '-D', '/tmp/tracer-_jmzdqg5.trace', '-E', 'LD_PRELOAD="./lib/libdl.so.0
   ...: "', '--', './upnpd'])
Out[4]: <subprocess.Popen at 0x7f6b7ee97430>

In [5]: ./upnpd: can't load library '"./lib/libdl.so.0"'
```
不太知道应该是什么问题了
试一试用shell=True反而成功了

```bash
>>> subprocess.Popen('sudo chroot /tmp/tracer_target_txip6ek6/CVE-2021-27329 ./shellphish-qemu-linux-arm -C /tmp/tracer_target_txip6ek6 -d nochain,exec,page,strace -D /tmp/tracer-_jmzdqg5.trace -E LD_PRELOAD="./nvram.so ./lib/libdl.so.0" -- ./upndp',shell=True)
<subprocess.Popen object at 0x7f8c73f19eb0>
>>> [0x00026460] fopen('/var/run/upnpd.pid', 'wb+') = 0x000e4008
[0x0002648c] custom_nvram initialised
[0x3ff60cb8] fopen('/tmp/nvram.ini', 'r') = 0x000e4008
[nvram 0] upnpd_debug_level = 9
[nvram 1] lan_ipaddr = 127.0.0.1
[nvram 2] hwrev = MP1T99
[nvram 3] hwver = R8500
[nvram 4] friendly_name = R8300
[nvram 5] upnp_enable = 1
[nvram 6] upnp_turn_on = 1
[nvram 7] upnp_advert_period = 30
[nvram 8] upnp_advert_ttl = 4
[nvram 9] upnp_portmap_entry = 1
[nvram 10] upnp_duration = 3600
[nvram 11] upnp_DHCPServerConfigurable = 1
[nvram 12] wps_is_upnp = 0
[nvram 13] upnp_sa_uuid = 00000000000000000000
[nvram 14] lan_hwaddr = AA:BB:CC:DD:EE:FF
[nvram 15] lan_hwaddr = 
Read 16 entries from /tmp/nvram.ini
acosNvramConfig_get('upnpd_debug_level') = '9'
[0x0002652c] acosNvramConfig_get('upnpd_debug_level') = '9'
set_value_to_org_xml:1149()
[0x0000e1e8] fopen('/www/Public_UPNP_gatedesc.xml', 'rb') = 0x000e4008
[0x0000e220] fopen('/tmp/upnp_xml', 'wb+') = 0x000e4008
data2XML()
[0x0000f520] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
[0x0000f4ec] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x0000f4b0] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x0000f014] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x3fe9ab64] open('/dev/mtdblock4', 0) = -1
open: No such file or directory
xmlValueConvert()
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
xmlValueConvert()
[0x0000f47c] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
xmlValueConvert()
[0x0000f4b0] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x0000f014] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x3fe9ab64] open('/dev/mtdblock4', 0) = -1
open: No such file or directory
xmlValueConvert()
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
xmlValueConvert()
[0x0000f47c] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
xmlValueConvert()
[0x0000f4b0] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x0000f014] acosNvramConfig_get('friendly_name') = 'R8300'
xmlValueConvert()
[0x3fe99838] acosNvramConfig_get('hwrev') = 'MP1T99'
xmlValueConvert()
[0x3fe9ab64] open('/dev/mtdblock4', 0) = -1
open: No such file or directory
xmlValueConvert()
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
upnp_uuid_generator:421()
[0x0000da1c] acosNvramConfig_get('lan_hwaddr') = 'AA:BB:CC:DD:EE:FF'
xmlValueConvert()
[0x0000f47c] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
xmlValueConvert()
parse_root_device_description:619()
[0x0000ce5c] fopen('/tmp/upnp_xml', 'rb') = 0x000e4008
findtok:519()
parse_root_device_description:644(), findtok(URLBase)
parse_root_device_description:681(), device_type0 = InternetGatewayDevice
findtok:519()
parse_root_device_description:752(), findtok(SCPDURL)
parse_root_device_description:780(): find EventSub URL
parse_root_device_description:795(): find </service>
parse_root_device_description:806(): assign the rest value of the service structure
[0x0000d3b4] acosNvramConfig_get('upnp_duration') = '3600'
parse_root_device_description:836(): assign the rest value of the device structure
[0x0000d454] acosNvramConfig_get('upnp_duration') = '3600'
emb_num = 0, max_layer = 1, layer = 1parse_root_device_description:687(), device_type0 = WANDevice
findtok:519()
parse_root_device_description:752(), findtok(SCPDURL)
parse_root_device_description:780(): find EventSub URL
parse_root_device_description:795(): find </service>
parse_root_device_description:806(): assign the rest value of the service structure
[0x0000d568] acosNvramConfig_get('upnp_duration') = '3600'
parse_root_device_description:836(): assign the rest value of the device structure
[0x0000d664] acosNvramConfig_get('upnp_duration') = '3600'
emb_num = 1, max_layer = 2, layer = 2parse_root_device_description:687(), device_type1 = WANConnectionDevice
findtok:519()
parse_root_device_description:752(), findtok(SCPDURL)
parse_root_device_description:780(): find EventSub URL
parse_root_device_description:795(): find </service>
parse_root_device_description:806(): assign the rest value of the service structure
[0x0000d568] acosNvramConfig_get('upnp_duration') = '3600'
findtok:519()
parse_root_device_description:752(), findtok(SCPDURL)
parse_root_device_description:780(): find EventSub URL
parse_root_device_description:795(): find </service>
parse_root_device_description:806(): assign the rest value of the service structure
[0x0000d568] acosNvramConfig_get('upnp_duration') = '3600'
findtok:519()
parse_root_device_description:752(), findtok(SCPDURL)
parse_root_device_description:780(): find EventSub URL
parse_root_device_description:795(): find </service>
parse_root_device_description:806(): assign the rest value of the service structure
[0x0000d568] acosNvramConfig_get('upnp_duration') = '3600'
parse_root_device_description:836(): assign the rest value of the device structure
[0x0000d664] acosNvramConfig_get('upnp_duration') = '3600'
emb_num = 2, max_layer = 3, layer = 3[0x00026504] [nvram 9] acosNvramConfig_set('upnp_portmap_entry', '0')
upnp_main:751()
[0x0001d084] acosNvramConfig_match('upnp_turn_on', '1') = 1
upnp_main: 763()
[0x0001d0a8] acosNvramConfig_match('lan_ipaddr', '0.0.0.0') = 0
create_received_scoket:158()
upnp_sockeInit:964()
[0x0001c4c4] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
create_submit_scoket: 199()
create_received_scoket:158()
upnp_sockeInit:964()
get_if_addr(946): Can't get IP from br0.
[0x0001d16c] acosNvramConfig_get('upnp_duration') = '3600'
upnp_duration is 3600
ssdp_discovery_advertisement(568):
rootDeviceOK(68):
ssdp_discovery_send(511):
ssdp_root_device_discovery_send(244):
igd_ssdp_root_device_discovery(84)
ssdp_packet(98):
[0x000247bc] acosNvramConfig_get('upnp_duration') = '3600'
http_mu_send:1041()
[0x0001dac8] acosNvramConfig_get('upnp_advert_ttl') = '4'
[0x0001dafc] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
UPNP: error to use LAN IP to be the interface of sending mulitcast datagram!
ssdp_UUID_discovery_send(477):
igd_ssdp_UUID_discovery(100)
ssdp_packet(98):
[0x000247bc] acosNvramConfig_get('upnp_duration') = '3600'
http_mu_send:1041()
[0x0001dac8] acosNvramConfig_get('upnp_advert_ttl') = '4'
[0x0001dafc] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
UPNP: error to use LAN IP to be the interface of sending mulitcast datagram!
ssdp_schemas_discovery_send(495):
igd_ssdp_schemas_discovery(125)
ssdp_packet(98):
[0x000247bc] acosNvramConfig_get('upnp_duration') = '3600'
http_mu_send:1041()
[0x0001dac8] acosNvramConfig_get('upnp_advert_ttl') = '4'
[0x0001dafc] acosNvramConfig_get('lan_ipaddr') = '127.0.0.1'
UPNP: error to use LAN IP to be the interface of sending mulitcast datagram!
[0x0001d1a0] acosNvramConfig_get('upnp_advert_period') = '30'
[0x0001d200] acosNvramConfig_match('upnp_turn_on', '1') = 1
```
难道是subprocess解释错了？？？？
哦哦，应该是不能用空格吧，这个list项`LD_PRELOAD="./nvram.so ./lib/libdl.so.0"`
应该就是这个问题
最终改成了这样就可以了,不需要加愚蠢的""可以

```python
subprocess.Popen(['sudo', 'chroot', '/tmp/tracer_target_txip6ek6/CVE-2021-27329', './shellphish-qemu-linux-arm', '-C', '/tmp/tracer_target_txip6ek6', '-d', 'nochain,exec,page,strace', '-D', '/tmp/tracer-_jmzdqg5.trace', '-E', 'LD_PRELOAD=./nvram.so ./lib/libdl.so.0',  '--', './upnpd'])
```

这样就可以顺利运行起来固件了


Trace一下在哪里开action的

应该需要修改下每次crash的时候默认的action，更改为访问127.0.0.1:5000端口

```python
ipdb> flight.actions
[<rex.exploit.actions.RexOpenChannelAction object at 0x7fb90085b640>, <rex.exploit.actions.RexSendAction object at 0x7fb900751340>]
```

```bash
  /home/wsl/angr-dev/rex/rex/crash.py(1039)_initialize()
   1038         # concrete tracing for coredump file?
-> 1039         self.concrete_trace()
   1040         self.create_project()

  /home/wsl/angr-dev/rex/rex/crash.py(433)concrete_trace()
    432         # NOTE: concrete_trace
--> 433         self.trace_result, self.core_registers = self.tracer.concrete_trace(testcase, channel,
    434                                                                              self.pre_fire_hook,

  /home/wsl/angr-dev/rex/rex/crash_tracer/full_tracer.py(17)concrete_trace()
     16         # Run qemu to collect coredump file for further analysis
---> 17         r = self.tracer_bow.fire(testcase=testcase, channel=channel, save_core=True, record_magic=self._is_cgc,
     18                                  pre_fire_hook=pre_fire_hook, delay=delay, actions=actions, taint=taint)

  /home/wsl/angr-dev/rex/archr/analyzers/__init__.py(71)fire()
     70                 pre_fire_hook(self, flight, channel=channel, testcase=testcase)
---> 71             self._fire_testcase(flight, channel=channel)
     72 

  /home/wsl/angr-dev/rex/archr/analyzers/__init__.py(76)_fire_testcase()
     75     def _fire_testcase(self, flight, channel=None): #pylint:disable=no-self-use
---> 76         flight.start()
     77         return flight.get_channel(channel)

  /home/wsl/angr-dev/rex/archr/targets/flight.py(98)start()
     97         for act in self.actions:
---> 98             act.perform()
     99 

> /home/wsl/angr-dev/rex/archr/targets/actions.py(70)perform()
     69         import ipdb; ipdb.set_trace();
---> 70         if not self.interaction:
     71             raise ActionError("No interaction context to perform %s" % self.__class__)
```

一开始这个在rex.crash_tracer中就把actions指定好了
我试试修改一下那个位置
看懂了它应该是默认支持tcp/udp的channel
找到了一个示例(test_***.py)都是非常重要的

```python
def test_linux_network_stacksmash_64():
    # Test exploiting a simple network server with a stack-based buffer overflow.
    inp = b'\x00' * 500
    lib_path = os.path.join(bin_location, "tests/x86_64")
    # ld_path = os.path.join(lib_path, "ld-linux-x86-64.so.2")
    path = os.path.join(lib_path, "network_overflow")
    port = random.randint(8000, 9000)
    with archr.targets.LocalTarget([path, str(port)], path,
                                   target_arch='x86_64',
                                   ipv4_address="127.0.0.1",
                                   tcp_ports=(port,)).build().start() as target:
        crash = rex.Crash(target, crash=inp, rop_cache_path=os.path.join(cache_location, 'network_overflow_64'),
            aslr=False,
            input_type=rex.enums.CrashInputType.TCP, port=port)

        exploit = crash.exploit(cmd=b"echo hello", blacklist_techniques={'ret2libc'})

        assert 'call_shellcode' in exploit.arsenal

        _check_arsenal_has_send(exploit.arsenal)

        # let's actually run the exploit

    new_port = random.randint(9001, 10000)
    with archr.targets.LocalTarget([path, str(new_port)],
                                   path,
                                   target_arch='x86_64',
                                   ipv4_address="127.0.0.1",
                                   tcp_ports=(new_port,)).build().start() as new_target:
        try:
            new_target.run_command("")

            # wait for the target to load
            time.sleep(.5)

            temp_script = tempfile.NamedTemporaryFile(suffix=".py", delete=False)
            exploit_location = temp_script.name
            temp_script.close()

            exploit.arsenal['call_shellcode'].script(filename=exploit_location)

            exploit_result = subprocess.check_output(["python", exploit_location,
                                                      "127.0.0.1", str(new_port),
                                                      ], timeout=3)
            assert b"hello" in exploit_result
        finally:
            os.unlink(exploit_location)


```

看一下并跑一下这个例子

模仿这个写一下

不太对，已经乱了，在胡搞....

## 修改日志 2023.03.09 zjz
跑通了利用tcp通信造成network_overflow 的crash,并且能正常生成trace文件和崩溃后的coredump，具体模拟的指令在qemu_tracer.py的167-175行（is_simulated=False）：
  (说明用nc的tcp和udp方式与进程通信的代码可行)
```python
fire_tmp_path = tmpdir + "/tests/fire"
aaa = ["sudo", fire_tmp_path]
target_cmd.pop(0)
target_cmd[0] = tmpdir + "/tests/shellphish-qemu-linux-x86_64"
aaa.extend(target_cmd)
print(aaa)
target_cmd = aaa
```

ps：用的是绝对路径，用chroot的话会有各种依赖问题，解决了也没什么意义

利用upd通信跑通了upnpd，用了chroot，并且能造成crash，但是在chroot中不能用fire，把fire内的命令拿出来用下面的指令执行后：
```python
self.target.run_command(['ulimit', '-c', 'unlimited'],is_shell=True).wait()
self.target.run_command(['ulimit', '-f', 'unlimited'],is_shell=True).wait()
```

因为ulimit -c unlimited只在当前会话生效，运行完该条指令后实效，因此后面运行：
```python
aaa = ["sudo", "chroot", self.target.target_cwd]
target_cmd.pop(0)
target_cmd[0] = "./shellphish-qemu-linux-arm"
aaa.extend(target_cmd)
print(aaa)
target_cmd = aaa
```
并不能生成coredump。

把命令拼接在一起，aaa = ["ulimit","-c", "unlimited", "&&", "sudo", "chroot", self.target.target_cwd]还是无法正确执行，因为通过subprocess.Popen 来执行ulimit -c unlimited，需要设置shell=True，而执行target_cmd需要shell=False，因此目前还不能找到在同一个会话中执行ulimit -c unlimited 和target_cmd的方法，coredump无法生成
ps：我在run_command()函数增加了is_shell参数，如默认为false，该参数最终传递给subprocess.Popen的shell参数，可以控制subprocess.Popen(shell=True) 或者subprocess.Popen(shell=True)

实现了is_simulated：
我在crash类和qemu_tracer中加入了is_simulated变量，默认取值为False。使用方法如下：
```python
crash = rex.Crash(target, crash=inp, input_type=rex.enums.CrashInputType.UDP, port=1900, is_simulated=True)
```
is_simulated的取值影响到程序运行的方式，如果是利用shellphish-qemu-linux-arm，且用chroot的方式方针程序，则设置is_simulated=True
```python
if self.is_simulated:
    self.target.run_command(['ulimit', '-c', 'unlimited'],is_shell=True).wait()
    self.target.run_command(['ulimit', '-f', 'unlimited'],is_shell=True).wait()
    #self.target.run_command(['exec', ""],is_shell=True).wait()
    
    aaa = ["sudo", "chroot", self.target.target_cwd]

    target_cmd.pop(0)
    target_cmd[0] = "./shellphish-qemu-linux-arm"

    aaa.extend(target_cmd)
    print(aaa)
    target_cmd = aaa
else:
    
    
    fire_tmp_path = tmpdir + "/tests/fire"
    aaa = ["sudo", fire_tmp_path]

    target_cmd.pop(0)
    target_cmd[0] = tmpdir + "/tests/shellphish-qemu-linux-x86_64"

    aaa.extend(target_cmd)
    print(aaa)
      target_cmd = aaa
```

注意CVE-2021-27329需要对libc.so.0进行patch。
如果不进行patch的话，qemu跑起程序后原进程会正常退出（returncode=0），然后固件跑在frok的一个新进程上。
patch以后就会一直运行在原进程上（libc.so.0已经替换成patch后的了）
CVE-2021-27329的原poc是PSV-2020-0211的poc，虽然也能造成程序的退出，但是无法crash。（我已经将poc修改成该漏洞的poc了）

## 修改日志 lxw 20230312

决定在里面放一个busybox来作为基础环境，这样应该fire就能够运行了,优雅很多

但是目前放入整个busybox环境后每次启动在我的环境下会segment

```bash
vmware@ubuntu2004:~$ sudo chroot /home/vmware/rexxx/my_binaries/CVE-2021-27329/ ./shellphish-qemu-linux-arm -g 1234 -C nochain,exec,page,strace -D /tmp/tracer-y[0/0]
46.trace -E LD_PRELOAD=./nvram.so ./lib/libdl.so.0 -- ./upnpd 1900
Segmentation fault
```

需要联调一下确定一下问题，crash的位置在ld中非常奇怪

确定是引号造成的问题

必须要加上引号

```sh
vmware@ubuntu2004:~$ sudo chroot /home/vmware/rexxx/my_binaries/CVE-2021-27329/ ./shellphish-qemu-linux-arm -g 1234 -C nochain,exec,page,strace -D /tmp/tracer-y[0/0]
46.trace -E LD_PRELOAD="./nvram.so ./lib/libdl.so.0" -- ./upnpd 1900
```

修复好了，用shell=True来运行`subprocess`,在`LD_PRELOAD`位置上加入双引号，就可以现在顺利运行

之后修改core文件的位置，让程序在chroot下顺利找到corefile

```python
if self.is_simulated:
  chroot_dir = os.path.split(self.target.target_cwd)[-1]
  target_cores = glob.glob(os.path.join(local_tmpdir,'*',chroot_dir,'qemu_*.core'))
```

之后在修改tracefile文件的位置，让程序顺利找到tracefile
就是把我们chroot的地址给它们去找即可

```python
if self.is_simulated:
    chroot_path = self.target.target_cwd
    chroot_dir = os.path.split(chroot_path)[-1]
    target_trace_filename = chroot_path+tmp_prefix + ".trace" if record_trace else None
    target_magic_filename = chroot_path+tmp_prefix + ".magic" if record_magic else None

```


### TODO
发现一个问题就是，实际设备的启动需要时间，如果我们很快去连接的话，就会造成发送的数据没有接收到，最后设备没有crash
这样的话，就会Timeout Error
所以需要在启动flight之后，停一会再action
这个地方我还没有考虑好加在哪里
搞定了，加了个sleep在执行完命令后


这些folder包括,每一个都有作用，弄来弄去的非常烦人

archr_local
archr_target
tracer_target

还需要修改angr_project.py

现在问题是不知道删了什么文件导致后面没办法执行，所以我在所有文件操作的函数上做了个简单的HOOK打印一下LOG
看一下文件变化的情况
好像他们的target_path就是原来的目录target的位置，比如 `/home/vmware/angr-dev/rexxx/tests/../my_binaries/tests/network_overflow`
但是我们需要在`/tmp`目录下做chroot,这个目录肯定是不太行的

所以target_path之类的都是不要改动的，我们只需要改动chroot运行后的目录和生成的core,tracer的目录

在后面
调用`self.analyzer._build_command`的位置还是会继续崩溃

```sh
ipdb> list
    157 
    158     def run_shellcode(self, shellcode, aslr=False, **kwargs):
    159         exit_code = 42
    160 
    161         # build the args
--> 162         if self.analyzer:
    163             args = self.analyzer._build_command()
    164         else:
    165             args = self.target.target_args
    166 
    167         # run command within the shellcode context
```

并且不能在这个过程中把我们的目录删掉，因为我们所有的binary都是从这个目录加载的

已经到了`create_project`位置

基本改到datascout.py位置了，又卡住了，可能是在
`output, stderr = p.communicate()`的位置卡住的

看一下目前的调用栈，这个函数的目的是要获取到运行时地址maps,这样的话就必须
运行程序，然后注入shellcode来运行
```py
  /home/vmware/angr-dev/rexxx/rex/crash.py(1043)_initialize()
   1042         self.concrete_trace()
-> 1043         self.create_project()
   1044         self.initialize_project()

  /home/vmware/angr-dev/rexxx/rex/crash.py(501)create_project()
    500 
--> 501         self.project = self.tracer.create_project(self.target)
    502 

  /home/vmware/angr-dev/rexxx/rex/crash_tracer/full_tracer.py(27)create_project()
     26         self._init_angr_project_bow(target)
---> 27         self.project = self.angr_project_bow.fire()
     28         return self.project

> /home/vmware/angr-dev/rexxx/archr/analyzers/angr_project.py(107)fire()
    106         if self._mem_mapping is None and self.scout_analyzer is not None:
--> 107             _,_,_,self._mem_mapping = self.scout_analyzer.fire()
    108 

  /home/vmware/angr-dev/rexxx/archr/analyzers/datascout.py(195)fire()
    194         if not self.argv:
--> 195             output = self.run_shellcode(self.read_file_shellcode("/proc/self/cmdline"), aslr=aslr, **kwargs)
    196             self.argv = output.split(b'\0')[:-1]

  /home/vmware/angr-dev/rexxx/archr/analyzers/datascout.py(171)run_shellcode()
    170                     if p.returncode != exit_code:
--> 171                         raise ArchrError("DataScout failed to get info from the target process.\n"
    172      

```
确定两件事：
1. 程序顺利运行了（确认）
2. shellcode顺利运行了
3. 程序能够顺利被Terminate

执行shellcode的方式是
A context that runs the target with shellcode injected over the entrypoint.
Useful for operating in the normal process context of the target.
在entrypoint的地方 插入shellcode(hook)
这个....能成功么
只能调试一下看看

```sh
ipdb> len(hooked_binary)
540644
ipdb> list
    431         if addr is None:
    432             hooked_binary = hook_entry(original_binary, asm_code=asm_code, bin_code=bin_code)
    433         else:
    434             hooked_binary = hook_addr(original_binary, addr, asm_code=asm_code, bin_code=bin_code)
    435
--> 436         with self.replacement_context(self.target_path, hooked_binary, saved_contents=original_binary):
    437             with self.run_context(*args, **kwargs) as p:
    438                 yield p
    439
    440     def run_command(
    441         self, args=None, args_prefix=None, args_suffix=None, env=None,is_shell=False, # for us

ipdb> len(hooked_binary)
540644
ipdb> len(original_binary)
540644
```

看起来执行前后，这个binary没有任何变化，这也和binary顺利运行相互印证,shellcode 并没有顺利运行
调试`hook_entry`

但是看了一下前后，应该是hook成功了呀
```sh
vmware@ubuntu2004:~$ md5sum /home/vmware/angr-dev/rexxx/my_binaries/CVE-2021-27329/upnpd
8650e9ad331bf6335f1c5bea407ee5b3  /home/vmware/angr-dev/rexxx/my_binaries/CVE-2021-27329/upnpd


```

这是hook后插入在`_start`入口的代码

```c
void start()
{
  int v0; // r8
  size_t v1; // r9
  ssize_t v2; // r0
  int v3; // r3
  int v4; // [sp-1018h] [bp-1018h] BYREF
  char v6[20]; // [sp-18h] [bp-18h] BYREF
  int v7; // [sp-4h] [bp-4h]

  v7 = 0;
  v6[19] = 0;
  strcpy(v6, "/proc/self/cmdline");
  v0 = linux_eabi_syscall(__NR_open, v6, 0, 0);
  do
  {
    v1 = linux_eabi_syscall(__NR_read, v0, &v4, 0x1000u);
    v2 = linux_eabi_syscall(__NR_write, 1, &v4, v1);
  }
  while ( v1 );
  linux_eabi_syscall(__NR_exit, 42);
  if ( !v3 )
    __asm { POPEQ           {R3,PC} }
  __asm { POPEQ           {R3,PC} }
}
```

就是读读读入口地址之类的，但是要注意这个target_path还是在我们的原来的路径
`/home/vmware/angr-dev/rexxx/my_binaries/CVE-2021-27329/upnpd`
但是我们run_command的时候运行的是我们在`/tmp/tracer_target_*`中chroot环境中的upnpd呀
所以运行结果失败了

好吧（我是真的想把这部分全部重构掉!）

所以要改,在修改了之后重新拷贝一下我们的target_path也就是upnpd
在这里，我们用replacement_context替换的是target_path的binary，过之后我们chroot执行的却不是，这里需要改掉
但是怎么改呢，我们都不知道....什么变量都没传到`<class 'archr.targets.local_target.LocalTarget'>`里面呀
头大

```py
        with self.replacement_context(self.target_path, hooked_binary, saved_contents=original_binary):
            with self.run_context(*args, **kwargs) as p:
                yield p
```


`\tmp`目录的部分垃圾不会回收,需要考虑把它们删除

但是好像hook_binary运行有问题
找到问题了是因为这段注入的shellcode中打开了`/proc/self/cmdline`
但是我们在chroot环境里是没有这个路径的，所以打开失败，最后整个injection都失败了



