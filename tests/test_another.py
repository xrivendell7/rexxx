import os
import random
import subprocess
import sys
import tempfile
import time
import struct
import logging

# Make sure to use our own code
sys.path.insert(0,"/home/wsl/angr-dev/rex/")
# print(sys.path)
# sys.path.insert(1,"../../")

import rex
import archr
import colorguard
from rex.vulnerability import Vulnerability
from angr.state_plugins.trace_additions import FormatInfoStrToInt, FormatInfoDontConstrain
from rex.exploit.cgc.type1.cgc_type1_shellcode_exploit import CGCType1ShellcodeExploit

from IPython import embed

bin_location = str(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../my_binaries'))
cache_location = str(os.path.join(bin_location, 'tests_data/rop_gadgets_cache'))
tests_dir = str(os.path.dirname(os.path.realpath(__file__)))

def _do_pov_test(pov, enable_randomness=True):
    ''' Test a POV '''
    for _ in range(10):
        if pov.test_binary(enable_randomness=enable_randomness):
            return True
    return False

def _check_arsenal_has_send(arsenal):
    # Test that the script generated for the arsenal has sends (i.e. is not null)
    for exploit in arsenal.values():
        assert ".send(" in exploit.script()

def test_linux_armel():
    path = bin_location
    bin_path = os.path.join(path, "vuln_stacksmash")
    ld_path = os.path.join(path, "ld-linux.so.3")
    libc_path = os.path.join(path,"libc.so.6")

    print('cache_location: ', cache_location)

    lib_path = path
    inp = b"A" * 227
    with archr.targets.LocalTarget([ld_path, '--library-path', lib_path, bin_path], bin_path, target_arch='arm').build().start() as target:
        print("Success")
        crash = rex.Crash(target, inp)

        # exploit = crash.exploit()
        embed()

def test_linux_armel_stacksmash_shell():
    path = bin_location
    bin_path = os.path.join(path, "vuln_stacksmash_withshell")
    ld_path = os.path.join(path, "ld-linux.so.3")
    libc_path = os.path.join(path,"libc.so.6")

    lib_path = path
    inp = b"A" * 0x100

    input("Let's go to run "+str(bin_path))
    with archr.targets.LocalTarget([ld_path, '--library-path', lib_path, bin_path], bin_path, target_arch='arm').build().start() as target:
        print("Success")
        crash = rex.Crash(target, inp)

        # exploit = crash.exploit(whitelist_techniques={"oneshot"})
        # payload = exploit.arsenal['oneshot'].dump()

if __name__ == "__main__":
    logging.getLogger("rex").setLevel("DEBUG")
    logging.getLogger("povsim").setLevel("DEBUG")
    logging.getLogger('archr').setLevel('DEBUG')
    logging.getLogger("angr.state_plugins.preconstrainer").setLevel("DEBUG")
    logging.getLogger("angr.simos").setLevel("DEBUG")
    logging.getLogger("angr.exploration_techniques.tracer").setLevel("DEBUG")
    logging.getLogger("angr.exploration_techniques.crash_monitor").setLevel("DEBUG")

    # if len(sys.argv) > 1:
    #     globals()['test_' + sys.argv[1]]()
    # else:
    #     run_all()
    # print(bin_location)
    test_linux_armel_stacksmash_shell()
    